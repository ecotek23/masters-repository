#include <thrust/execution_policy.h>
#include <thrust/host_vector.h>
#include <thrust/copy.h>

#include <curand_kernel.h>
#include <curand.h>

#include <algorithm>
#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <vector>
#include <random>
#include <math.h>

#include "data_generation_generic.h"

// importance weight function
float weight(float y_k,float x_km1,float sig_v,float sig_w) {
	float val = expf(-0.5*powf(y_k-x_km1,2)/(sig_v+sig_w));
	return val;
}

// sig_k calculator for this model
float sig_calculator(float sig_w,float sig_v) {
	float sum = (1/sig_w)+(1/sig_v);
	float inverse = 1/sum;
	return inverse;
}

// m_k calculator for this model
float m_calculator(float x_km1, float y_k, float sig_w, float sig_v) {
	float val = sig_calculator(sig_w,sig_v)*(x_km1/sig_v+y_k/sig_w);
	return val;
}

int main( void ) {
	// Initialize random number generator
	std::srand(time(0));
	
	// Model specific sigma values
	float sig_w = 1;
	float sig_v = 1;
	// Number of simulations from paper (100)
	int M = 100;
	// Size of sample vectors to generate, same as value in the original paper (500)
	int K = 500;
	// Size of particle simulations to run (100)
	int N = 100;
    
    // Probably don't need to save the y values since they're not used in later computation, but will for now
    float true_values[2][M][K]; // 0 for x, 1 for y
    
    float x_host_vector[K], y_host_vector[K];
	float *x_device_vector;
	float *y_device_vector;
	curandState_t* states;
	
	// Allocate space on device
	size_t bytes = K*sizeof(float);
	cudaMalloc(&x_device_vector,bytes);
	cudaMalloc(&y_device_vector,bytes);
	cudaMalloc((void**) &states, K * sizeof(curandState_t));

	// Allocate vector used for inclusive scan
	thrust::host_vector<float> x_vector(K);

	// Initialize GPU RNG
	init<<<K, 1>>>(rand(), states);
	
	// Allocate variables for holding simulation data, M vectors of N vectors of K length
	std::vector<std::vector<std::vector<float>>> x_sims(M);
	std::vector<std::vector<std::vector<float>>> weight_sims(M);
	std::vector<std::vector<std::vector<float>>> normalised_weight_sims(M);
	
	std::default_random_engine generator;
	std::normal_distribution<float> distribution(0.0f,1.0f);
	
	for (int m=0;m<M;m++) {
		// Ensure there is enough space for simulating data and initialize as necessary
		x_sims[m].resize(N);
		weight_sims[m].resize(N);
		normalised_weight_sims[m].resize(N);
		
		for (int n=0;n<N;n++) {
			x_sims[m][n].resize(K);
			weight_sims[m][n].resize(K);
			normalised_weight_sims[m][n].resize(K);
			
			// Initialize x_0 for each simulation
			x_sims[m][n][0] = distribution(generator);
			
			// Initialize weights for each simulation
			weight_sims[m][n][0] = 1.0f/N;
			normalised_weight_sims[m][n][0] = 1.0f/N;
		}
		
	}
	
	float m_calc;
	float weight_sum = 0;
	float sig_calc = sig_calculator(sig_w,sig_v); // Remains the same, so no need to recalculate each run
	float weights[N];
	
	for (int m=0;m<M;m++) { // SIS method with optimal importance function
		normal_populate<<<K,1>>>(x_device_vector,K,states);
		normal_populate<<<K,1>>>(y_device_vector,K,states);
	
		// Retrieve generated data from device
		cudaMemcpy(x_host_vector,x_device_vector,bytes,cudaMemcpyDeviceToHost);
		cudaMemcpy(y_host_vector,y_device_vector,bytes,cudaMemcpyDeviceToHost);

//
// TESTING SECTION
for (int i=0;i<K;i++) {
	std::cout << x_host_vector[i] << std::endl << y_host_vector[i] << std::endl;
}
std::cout << "Done displaying generated data, bye!" << std::endl;
exit(0);
//
//

		
		// inclusive_scan to add up compounded effects on x, generating x data in the process
		for (int i=0;i<K;i++) {
			x_vector[i] = x_host_vector[i];
		}	
		thrust::inclusive_scan(thrust::host,x_vector.begin(),x_vector.end(),x_vector.begin());
	
		// generate y data
		for (int i=0;i<K;i++) { 
			y_host_vector[i] += x_host_vector[i];
		}
	
		for (int k=0;k<K;k++) {
			true_values[0][m][k] = x_vector[k];
			true_values[1][m][k] = y_host_vector[k];
		}
		
		for (int k=0;k<K;k++) {
			for (int n=0;n<N;n++) { // Simulate x value
				////////////////////////
				std::cout << x_sims[m][n][k] << std::endl; // LOOK AT THIS INDEXING
				////////////////////////
				m_calc = m_calculator(x_sims[m][n][k-1],y_host_vector[k],sig_w,sig_v);
				std::normal_distribution<float> distribution(m_calc,sig_calc);
				x_sims[m][n][k] = distribution(generator);
			}
			for (int n=0;n<N;n++) { // Calculate weights
				weight_sims[m][n][k] = weight(y_host_vector[k],x_sims[m][n][k-1],sig_v,sig_w);
			}
			
			for (int n=0;n<N;n++) { // Sum weights
				weight_sum += weight_sims[m][n][k];
			}
		
			for (int n=0;n<N;n++) { // Normalise weights
				normalised_weight_sims[m][n][k] = weight_sims[m][n][k]/weight_sum;
				weights[n] = normalised_weight_sims[m][n][k];
			}			
			
			float effective_sample_size = effective_sample_size_estimate(weights,K);
			/////////////////////////////////////////////////////////////////////
			std::cout << effective_sample_size << std::endl; // SOMETHING WRONG WITH effective_sample_size calculation
			/////////////////////////////////////////////////////////////////////
			if (effective_sample_size < N/3.0f) { // In the event of degenerative behaviour, implement resampling routine here
				std::cout << "Need to resample at m=" << m << " and k=" << k << std::endl;
				//std::cout << "NEED TO IMPLEMENT RESAMPLING IN THIS SECTION\nEXITING..." << std::endl;
				//exit(1);
			}
			
			weight_sum = 0;
		}
	}
	
	// Check if weights are being normalised properly
	float test = 0;
	int index_to_check = rand() % M;
	std::for_each(normalised_weight_sims[index_to_check].begin(),normalised_weight_sims[index_to_check].end(),[&test](std::vector<float> val) {test += val[432];});
	if (fabs(test-1) > 1e-6) {
		std::cout << test-1 << std::endl;
		std::cout << "Something is wrong with the weight normalisation" << std::endl;
		exit(1);
	}
	
	// Do variance estimate from the paper
	float var_sum = 0;
	float inner_m_sum = 0;
	for (int k=0;k<K;k++) {
		for (int m=0;m<M;m++) {
			
			float x_calc = 0;
			for (int n=0;n<N;n++) { // Calculation in paper for the strange value
				x_calc += normalised_weight_sims[m][n][k]*x_sims[m][n][k];
			}
			inner_m_sum += pow((x_calc-true_values[0][m][k]),2);
			
		}
		inner_m_sum /= (float)M; // Not sure if cast necessary but it's 4am and I'm not bothering to test that
		inner_m_sum = sqrtf(inner_m_sum);
		var_sum += inner_m_sum;
	}
	var_sum /= (float)K; // Not sure if cast necessary but it's 4am and I'm not bothering to test that
	std::cout << "The value of var_sum is:\n";
	std::cout << var_sum << std::endl;
	return 0;
}
