/*
 * 	Need to write calculate and implement routine for weighting the samples
 */

/////////////
//
// NEED TO ALTER DATA GENERATION ROUTINE FOR DIFFERENT INITIAL DISTRIBUTIONS
//
/////////////

#include <thrust/execution_policy.h>
#include <thrust/host_vector.h>
#include <thrust/copy.h>

#include <curand_kernel.h>
#include <curand.h>

#include <algorithm>
#include <iostream>
#include <math.h>

#include "data_generation_generic.h"

// CUDA non-linear function for x_data
	// TEST THIS
__device__ void f(float x,int k,float *val) {
	*val = (float)((1.0/2.0)*x+25.0*(x/(1+powf(x,2)))+8.0*cos(1.2*k));
}

// Non-linear function for x_data
float f(float x,int k) {
	float val = (1.0/2.0)*x+25.0*(x/(1+pow(x,2)))+8.0*cos(1.2*k);
	return val;
}

// CUDA non-linear function for y_data
	// TEST THIS
__global__ void g(float x,float *val) {
	*val = powf(x,2)/20;
}

// Non-linear function for y_data
float g(float x) {
	float val = powf(x,2)/20;
	return val;
}

// sig_k calculator for this model
float sig_calculator(float x_km1,float sig_w,float sig_v,int k) {
	float sum = (1/sig_v)+(1/sig_w)*(pow(f(x_km1,k),2)/100);
	float inverse = 1/sum;
	return inverse;
}

// m_k calculator for this model
float m_calculator(float x_km1,float y_k,float sig_w,float sig_v,int k) {
	float sig_k = sig_calculator(x_km1,sig_w,sig_v,k);
	float val = sig_k*((1/sig_v)*f(x_km1,k)+(1/sig_w)*(f(x_km1,k)/10)*(y_k+(pow(f(x_km1,k),2)/20)));
	return val;
}

int main( void ) {
	// Initialize random number generator
	std::srand(time(0));
	
	// Model specific sigma values
	float sig_v = 10.0f;
	float sig_w = 1.0f;
	// Number of simulations from paper
	float M = 100;
	// Size of sample vectors to generate, same as value in the original paper
	int n = 500;
	
    float *v_host_vector;
	float *v_device_vector;
	float x_data[n];
	float *w_host_vector;
	float *w_device_vector;
	float y_data[n];
	curandState_t* states;
    
    size_t bytes = n*sizeof(float);
    v_host_vector = (float*)malloc(bytes);
    w_host_vector = (float*)malloc(bytes);
	
	// Allocate space on device
	cudaMalloc(&v_device_vector,bytes);
	cudaMalloc(&w_device_vector,bytes);
	cudaMalloc((void**) &states, n * sizeof(curandState_t));

	// Run data generation routines
	init<<<n, 1>>>(time(0), states); // initialize necessary states vector
    normal_populate<<<n,1>>>(v_device_vector,n,states); // populate vector with curand_uniform
	normal_populate<<<n,1>>>(w_device_vector,n,states); // populate vector with curand_uniform
	
	// Retrieve generated data from device
    cudaMemcpy(v_host_vector,v_device_vector,bytes,cudaMemcpyDeviceToHost);
    cudaMemcpy(w_host_vector,w_device_vector,bytes,cudaMemcpyDeviceToHost);

	// Calculate x_data
	x_data[0] = v_host_vector[0];
	for (int i=1;i<n;i++) {
		float current_x_val = x_data[i-1];
		x_data[i] = f(current_x_val,i)+v_host_vector[i];
	}
	
	// Calculate y_data
	for (int i=0;i<n;i++) {
		y_data[i] = g(x_data[i])+w_host_vector[i];
	}
	
	// Display generated data
	display("x data",x_data,n);
	display("y data",y_data,n);
	
	// Allocate variables for holding simulation data
	std::vector<std::vector<float>> x_sims(M);
	std::vector<std::vector<float>> weight_sims(M);
	std::vector<std::vector<float>> normalised_weight_sims(M);
	
	std::default_random_engine generator;
	std::normal_distribution<float> distribution(0.0f,5.0f);
	
	for (int m=0;m<M;m++) {
		// Ensure there is enough space for simulating data and initialize necessary
		x_sims[m].resize(n);
		weight_sims[m].resize(n);
		normalised_weight_sims[m].resize(n);
		
		// Initialize x_0 for each simulation
		x_sims[m][0] = distribution(generator);
		
		// Initialize weights
		weight_sims[m][0] = 1.0f/M;
		normalised_weight_sims[m][0] = 1.0f/M;
	}
	
	// COULD MAYBE MOVE THIS SECTION DOWN TO THE WEIGHTS CHECK OUT INTO A SEPARATE FUNCTION, LARGELY REPLICATED IN A AND B
	float m_calc,sig_calc;
	float weight_sum = 0;
	for (int k=1;k<n;k++) { // SIS method with optimal importance function
		for (int m=0;m<M;m++) { // Simulate x value
			m_calc = m_calculator(x_sims[m][k-1],y_data[k],sig_w,sig_v,k);
			sig_calc = sig_calculator(x_sims[m][k-1],sig_w,sig_v,k);
			std::normal_distribution<float> distribution(m_calc,sig_calc);
			x_sims[m][k] = distribution(generator);
		}
		for (int m=0;m<M;m++) { // Calculate weights
			weight_sims[m][k] = weight(y_data[k],x_sims[m][k-1],sig_v,sig_w);
		}
		for (int m=0;m<M;m++) { // Sum weights together
			weight_sum += weight_sims[m][k];
		}
		for (int m=0;m<M;m++) { // Normalise weights
			normalised_weight_sims[m][k] = weight_sims[m][k]/weight_sum;
		}
		weight_sum = 0;
	}
	
	// Check if weights are being normalised properly
	float test = 0;
	std::for_each(normalised_weight_sims.begin(),normalised_weight_sims.end(),[&test](std::vector<float> val) {test += val[432];});
	if (fabs(test-1) > 1e-6) {
		std::cout << test-1 << std::endl;
		std::cout << "Something is wrong with the weight normalisation" << std::endl;
		exit(1);
	}
	
	// Do variance estimate from the paper
	float var_sum = 0;
	for (int k=0;k<n;k++) {
		for (int j=0;j<M;j++) {
			continue; // Calculate the x estimate and use it in the formula provided, then display result
		}
	}
	
	return 0;
}
