#include <thrust/execution_policy.h>
#include <thrust/host_vector.h>
#include <thrust/copy.h>

#include <curand_kernel.h>
#include <curand.h>

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <math.h>

// Data display utility functions
template <typename T>
void display(std::string name, T* input_vector, int N) {
	std::cout << name << std::endl;
	std::for_each(input_vector,input_vector+N,[](T val) { std::cout << val << std::endl; });
	std::cout << std::endl;
}
template <typename T>
void display(std::string name, std::vector<T> input_vector) {
	std::cout << name << std::endl;
	std::for_each(input_vector.begin(),input_vector.end(),[](T val) { std::cout << val << std::endl; });
	std::cout << std::endl;
}
// curand initialization routines for normal data generation
__global__ void init(unsigned int seed, curandState_t* states) {
	curand_init(seed,blockIdx.x,0,&states[blockIdx.x]);
}

__global__ void normal_populate(float *out,int N,curandState_t* states) {
	int id = blockIdx.x*blockDim.x+threadIdx.x;
	if (id < N) {
		out[id] = curand_normal(&states[id]);
	}
}
// Effective sample size calculation for both array and vector inputs
template <typename T>
T effective_sample_size_estimate(T *weights, int N) {
	T sum = 0;
	std::for_each(weights,weights+N,[&sum](T weight){sum += powf(weight,2);});
	sum = 1/sum;
	return sum;
}
template <typename T>
T effective_sample_size_estimate(std::vector<T> weights) {
	T sum = 0;
	std::for_each(weights.begin(),weights.end(),[&sum](T weight){sum += powf(weight,2);});
	sum = 1/sum;
	return sum;
}
// Normalising weights for array inputs, writing to new array due to often requiring unnormalised 
void normalize_weights(float *input, float *output, int N) {
	float sum = std::accumulate(input,input+N,0.0f);
	for (int i=0;i<N;i++) {
		output[i] = input[i]/sum;
	}
}
