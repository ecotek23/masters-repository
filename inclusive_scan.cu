#include <thrust/scan.h>
#include <thrust/execution_policy.h>
#include <thrust/sequence.h>
#include <thrust/host_vector.h>

#include <iostream>
using namespace std;

int main( void ) {
	thrust::host_vector<double> H(10);
	thrust::sequence(H.begin(),H.end());

	thrust::inclusive_scan(thrust::host,H.begin(),H.end(),H.begin());

	for (int i=0;i<10;i++) {
		cout << H[i] << " ";
	}
	cout << endl;

	return 0;
}