\contentsline {chapter}{\chapternumberline {1}Introduction}{3}{chapter.1}
\contentsline {subsection}{\numberline {1.0.1}Aims and scope of my project}{3}{subsection.1.0.1}
\contentsline {subsection}{\numberline {1.0.2}My Tools}{3}{subsection.1.0.2}
\contentsline {subsection}{\numberline {1.0.3}Organisation of this report}{3}{subsection.1.0.3}
\contentsline {chapter}{\chapternumberline {2}Bayesian Filtering and Particle Methods}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Particle Filtering}{6}{section.2.1}
\contentsline {section}{\numberline {2.2}Resampling}{6}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Linear Implementations}{6}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Parallel Implementations - TALK ABOUT CUDA HERE?}{6}{subsection.2.2.2}
\contentsline {chapter}{\chapternumberline {3}Computing on GPU's}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}What is a GPU?}{7}{section.3.1}
\contentsline {section}{\numberline {3.2}What is CUDA?}{7}{section.3.2}
\contentsline {section}{\numberline {3.3}My use of CUDA}{7}{section.3.3}
\contentsline {section}{\numberline {3.4}Maximizing Computational Power}{7}{section.3.4}
\contentsline {chapter}{\chapternumberline {4}Numerical examples in CUDA and CPU}{9}{chapter.4}
\contentsline {section}{\numberline {4.1}First example - Linear Gaussian Model}{9}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}The model specification}{9}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}My implementation}{9}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Kalman Filter - CPU}{9}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Kalman Filter - GPU}{10}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}Comparisons of statistics}{10}{subsection.4.1.5}
\contentsline {subsubsection}{Mean}{10}{section*.1}
\contentsline {subsubsection}{Covariance matrix}{10}{section*.2}
\contentsline {subsubsection}{Normalizing constant}{10}{section*.3}
\contentsline {section}{\numberline {4.2}Second Example - Non-linear Model}{10}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}The model specification}{10}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}My implementation}{11}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Comparisons of statistics}{11}{subsection.4.2.3}
\contentsline {subsubsection}{Mean}{11}{section*.4}
\contentsline {subsubsection}{Covariance matrix}{11}{section*.5}
\contentsline {subsubsection}{Normalizing constant}{11}{section*.6}
\contentsline {section}{\numberline {4.3}Third example - Lorenz96}{11}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}The model specification}{11}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}My implementation}{11}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Comparisons of statistics}{11}{subsection.4.3.3}
\contentsline {subsubsection}{Mean}{11}{section*.7}
\contentsline {subsubsection}{Covariance matrix}{11}{section*.8}
\contentsline {subsubsection}{Normalizing constant}{11}{section*.9}
\contentsline {chapter}{\chapternumberline {5}Conclusions and Results}{13}{chapter.5}
\contentsline {section}{\numberline {5.1}Avenues for further work}{13}{section.5.1}
\contentsline {appendix}{\chapternumberline {A}Additional}{15}{appendix.A}
\contentsline {section}{\numberline {A.1}INSERT MENDELEY FILE HERE FOR THE BIB}{15}{section.A.1}
