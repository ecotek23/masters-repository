#include <curand_kernel.h>
#include <curand.h>
#include <iostream>
using namespace std;

__global__ void adjacentDifference(double input_vector[],int N,double output_vector[]) {
        int id = blockIdx.x*blockDim.x+threadIdx.x;
        if (id == 0) {
                output_vector[0] = input_vector[0];
        }
        else if (id < N) {
                output_vector[id] = input_vector[id]-input_vector[id-1];
        }
}


__global__ void metropolis_ancestors(int N, double *weights, int B, int *a) {
	// TO-DO
	// TEST THIS FUNCTION, ENTIRELY UNTESTED WITH CURAND IMPLEMENTED

	curandState s;
	curand_init(1234,0,0,&s);

	int i = threadIdx.x + blockIdx.x*blockDim.x;
	// Need to ensure blocksize and gridsize are appropriate for N
	if (i < N) {
		int k = i;
		for (int n=0;n<B;n++) {
			float u = curand_uniform(&s);
			int j = (curand(&s)%N)+1;
			if (u < weights[j]/weights[k]) {
				k = j;
			}
		}
		a[i] = k;
	}
}

__global__ void ancestorsToOffspring(int *as,int *os,int as_size) {     // Also implemented in LibBi
	int id = blockIdx.x*blockDim.x+threadIdx.x;
	if (id < as_size) {
		os[id] = 0;
	}
	__syncthreads(); // Ensure initialization
	
	if (id < as_size) {
		atomicAdd(&os[as[id]],1); // Slower due to enforced atomicity
	}
}

__global__ void prepermute(int *a,int *d,int N) {			// Not sure if correct, suspect not
	int id = blockIdx.x*blockDim.x+threadIdx.x;
	if (id < N) {
		d[id] = N+1;
	}
	__syncthreads(); // Ensure all work is done initializing before continuing

	if (id < N) {
		atomicExch(&d[a[id]],atomicMin(&d[a[id]],id));
	}
}

int main( void ) {
/* TEST CODE SETUP */
        int n = 10;
        int *h_input_vector;
        int *h_output_vector;
        int *d_input_vector;
        int *d_output_vector;

        size_t bytes = n*sizeof(int);

        h_input_vector = (int*)malloc(bytes);
        h_output_vector = (int*)malloc(bytes);

        cudaMalloc(&d_input_vector,bytes);
        cudaMalloc(&d_output_vector,bytes);

        for (int i=0;i<n;i++) {
                h_input_vector[i] = rand() % n;
        }

        cudaMemcpy(d_input_vector,h_input_vector,bytes,cudaMemcpyHostToDevice);

        int blocksize, gridsize;
        blocksize = 1024;
        gridsize = (int)ceil((float)n/blocksize);
/* END TEST CODE SETUP */

        ancestorsToOffspring<<<gridsize,blocksize>>>(d_input_vector,d_output_vector,n);
//	prepermute<<<gridsize,blocksize>>>(d_input_vector,d_output_vector,n);

	cudaMemcpy(h_output_vector,d_output_vector,bytes,cudaMemcpyDeviceToHost);

        cout << "=========================================" << endl;
        cout << "Original vector" << endl;
        cout << "=========================================" << endl;
        for (int i=0;i<n;i++) {
                cout << i << " : " << h_input_vector[i] << endl;
        }

        cout << "=========================================" << endl;
        cout << "Ancestors to offspring vector" << endl;
        cout << "=========================================" << endl;
        for (int i=0;i<n;i++) {
                cout << i << " : " << h_output_vector[i] << endl;
	}
	return 0;
}