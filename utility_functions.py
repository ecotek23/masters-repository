def inclusive_prefix_sum(w):
	for i in range(1,len(w)):
		w[i]+=w[i-1]
	return w

def exclusive_prefix_sum(w): # INCORRECT
	W = [None]*len(w)
	for i in range(len(w)):
		if i==0:
			W[i] = 0
		else:
			W[i] = sum(w[:i-1])
	return W

def adjacent_difference(W):
	w = [None]*len(W)
	w[0] = W[0]
	for i in range(1,len(w)):
		w[i] = W[i]-W[i-1]
	return w

def cumulative_offspring_to_ancestors(offspring_vector,N): # INDEX ERRORS
	a = [None]*N
	o = [None]*N	# Initialise o and a vectors
	for i in range(1,N+1):
		if i==1:
			start=0
		else:
			start = offspring_vector[i-1]

		o[i] = offspring_vector[i]-start
		for j in range(1,o[i]):
			a[start+j] = i
	return a

def ancestors_to_offspring(ancestor_vector): # INDEX ERRORS
	N = len(ancestor_vector)	# retrieve dimension of ancestor vector
	o = [0]*(N+1)	# set o to be the zero vector of dimension N
	for i in range(1,N+1):
		o[ancestor_vector[i]] += 1	# atomic update operation, must consider race condition for parallel code
	return o