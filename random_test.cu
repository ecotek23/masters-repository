#include <curand_kernel.h>
#include <curand.h>
#include <cstdlib>

#include <iostream>
using namespace std;

__global__ void init(unsigned int seed, curandState_t* states) {
	curand_init(seed,blockIdx.x,0,&states[blockIdx.x]);
}

__global__ void random_populate(float *out,int N,curandState_t* states) {
	int id = blockIdx.x*blockDim.x+threadIdx.x;
	if (id < N) {
		out[id] = curand_uniform(&states[id]);
	}
}

int main( void ) {
	int n = 10;
        float *h_output_vector;
	float *d_output_vector;
	curandState_t* states;
        size_t bytes = n*sizeof(float);

        h_output_vector = (float*)malloc(bytes);
	
	cudaMalloc(&d_output_vector,bytes);
	cudaMalloc((void**) &states, n * sizeof(curandState_t));

	init<<<n, 1>>>(time(0), states); // initialize necessary states vector
        random_populate<<<n,1>>>(d_output_vector,n,states); // populate vector with curand_uniform
	
        cudaMemcpy(h_output_vector,d_output_vector,bytes,cudaMemcpyDeviceToHost);

	for (int i=0;i<n;i++) {
		cout << i << " : " << h_output_vector[i] << endl;
	}

	return 0;
}