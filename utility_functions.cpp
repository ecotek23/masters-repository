#include <iostream>
#include <vector>
using namespace std;

template <typename T>
vector<T> inclusive_prefix_sum(T input_vector[],int N) {
	vector<T> output_vector;
	output_vector.push_back(input_vector[0]);
	for (int i=1;i<N;i++) {
		output_vector.push_back(output_vector[i-1]+input_vector[i]);
	}
	return output_vector;
}

template <typename T>
vector<T> adjacent_difference(T input_vector[],int N) {
	vector<T> output_vector;
	output_vector.push_back(input_vector[0]);
	for (int i=1;i<N;i++) {
		output_vector.push_back(input_vector[i]-input_vector[i-1]);
	}
	return output_vector;
}

template <typename T>
void display_vector(vector<T> input_vector) {
	for (T i: input_vector) {
		cout << i << " ";
	}
	cout << endl;
}

template <typename T>
void permute(T *a,int N) { // from "Parallel resampling in the particle filter"
	for (int i=0;i<N;i++) {
		if (a[i]!=i && a[a[i]]!=a[i]) {
			T temp = a[i];
			a[i] = a[a[i]];
			a[a[i]] = temp;
			i--;
		}
	}
}

int main( void ) {
	double input_vector[] = {1.32,2.4332,3.234};
	vector<double> output_vector;

	cout << "==============================" << endl;
	cout << "Original vector" << endl;
	cout << "==============================" << endl;
	for (int i=0;i<3;i++) {
		cout << input_vector[i] << " ";
	}
	cout << endl << endl;
	
	output_vector = inclusive_prefix_sum(input_vector,3);
	cout << "==============================" << endl;
	cout << "Inclusive prefix sum" << endl;
	cout << "==============================" << endl;
	display_vector(output_vector);
	cout << endl;

	output_vector = adjacent_difference(input_vector,3);
	cout << "==============================" << endl;
	cout << "Adjacent difference" << endl;
	cout << "==============================" << endl;
	display_vector(output_vector);
	cout << endl;

	return 0;
}