#THRUST_PATH = /usr/local/include/thrust Placed on include path so not necessary to use

all:
	# C/C++ files
	g++ -std=c++11 -o utility_functions.out utility_functions.cpp
	# Python files
	python -m compileall .
	# Cuda files
	nvcc -Wno-deprecated-gpu-targets -o inclusive_scan_CUDA.out inclusive_scan.cu
	nvcc -Wno-deprecated-gpu-targets -o exclusive_scan_CUDA.out exclusive_scan.cu
	nvcc -arch=sm_20 -Wno-deprecated-gpu-targets -o utility_functions_CUDA.out utility_functions.cu
	nvcc -Wno-deprecated-gpu-targets -o random_test_CUDA.out random_test.cu
clear:
	rm -f *.out
	rm -f *.x
	rm -f *.pyc